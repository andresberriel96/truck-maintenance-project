--CREATE DB
CREATE DATABASE TruckMaintenance

--CREATE TABLES
USE TruckMaintenance
CREATE TABLE Truck (
	Id INT IDENTITY(1,1) PRIMARY KEY,
	Number INT,
	Model VARCHAR(50)
)

CREATE TABLE TypeMaintenance (
	Id INT IDENTITY(1,1) PRIMARY KEY,
	Description VARCHAR(100),
)

CREATE TABLE Driver (
	Id INT IDENTITY(1,1) PRIMARY KEY,
	Name VARCHAR(50),
	LastName VARCHAR(50),
	Age INT
)

CREATE TABLE Dispatcher (
	Id INT IDENTITY(1,1) PRIMARY KEY,
	Name VARCHAR(50),
	LastName VARCHAR(50)
)

CREATE TABLE Mechanical (
	Id INT IDENTITY(1,1) PRIMARY KEY,
	Name VARCHAR(50),
	LastName VARCHAR(50)
)

CREATE TABLE Maintenances (
	Id INT IDENTITY(1,1) PRIMARY KEY,
	TruckId INT FOREIGN KEY REFERENCES Truck(Id),
	TypeId INT FOREIGN KEY REFERENCES TypeMaintenance(Id),
	DriverId INT FOREIGN KEY REFERENCES Driver(Id),
	DispatcherId INT FOREIGN KEY REFERENCES Dispatcher(Id),
	DueDate Date,
	MechanicalId INT FOREIGN KEY REFERENCES Mechanical(Id)
)