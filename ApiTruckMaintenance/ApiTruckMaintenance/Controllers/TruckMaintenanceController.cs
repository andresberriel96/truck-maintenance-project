﻿using ApiTruckMaintenance.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data.Entity;

namespace ApiTruckMaintenance.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class TruckMaintenanceController : ControllerBase
	{
		TruckMaintenanceContext db = new TruckMaintenanceContext();

		[HttpGet]
		public List<TruckMaintenance> Get()
		{
			var maintenances = new List<TruckMaintenance>();
			var result = db.Maintenances.ToList();

			foreach (var item in result)
			{
				Truck truck = db.Trucks.Where(x => x.Id == item.TruckId).FirstOrDefault();
				TypeMaintenance type = db.TypeMaintenances.Where(x => x.Id == item.TypeId).FirstOrDefault();
				Driver driver = db.Drivers.Where(x => x.Id == item.DriverId).FirstOrDefault();
				Dispatcher dispatcher = db.Dispatchers.Where(x => x.Id == item.DispatcherId).FirstOrDefault();
				Mechanical mechanical = db.Mechanicals.Where(x => x.Id == item.MechanicalId).FirstOrDefault();

				TruckMaintenance maintenance = new TruckMaintenance();
				maintenance.Id = item.Id;
				maintenance.Truck = truck.Number.ToString();
				maintenance.Type = type.Description;
				maintenance.Driver = String.Format("{0} {1}", driver.Name, driver.LastName);
				maintenance.Dispatcher = String.Format("{0} {1}", dispatcher.Name, dispatcher.LastName);
				maintenance.DueDate = item.DueDate;
				maintenance.Mechanical = String.Format("{0} {1}", mechanical.Name, mechanical.LastName);
				maintenances.Add(maintenance);
			}
			return maintenances;
		}

		[HttpGet]
		[Route("{id}")]
		public Maintenance GetItem(int id)
		{
			return db.Maintenances.Where(x => x.Id == id).FirstOrDefault();
		}

		[HttpPut]
		[Route("Update")]
		public JsonResult update([FromBody] Maintenance maintenance)
		{
			var updateMaintenance = db.Maintenances.FirstOrDefault(x => x.Id == maintenance.Id);
			updateMaintenance.TruckId = maintenance.TruckId;
			updateMaintenance.TypeId = maintenance.TypeId;
			updateMaintenance.DriverId = maintenance.DriverId;
			updateMaintenance.DispatcherId = maintenance.DispatcherId;
			updateMaintenance.DueDate = maintenance.DueDate;
			updateMaintenance.MechanicalId = maintenance.MechanicalId;
			db.SaveChanges();
			return new JsonResult("Truck maintenance updated");
		}

		[HttpPost]
		[Route("Create")]
		public JsonResult Create([FromBody] Maintenance maintenance)
		{
			db.Maintenances.Add(maintenance);
			db.SaveChanges();
			return new JsonResult("Truck maintenance saved");
		}

		[HttpDelete]
		[Route("Delete/{id}")]
		public List<TruckMaintenance> Delete(int id)
		{
			var maintenances = new List<TruckMaintenance>();

			var itemDelete = db.Maintenances.Where(x => x.Id == id).FirstOrDefault();
			if (itemDelete != null)
			{
				db.Maintenances.Remove(itemDelete);
				db.SaveChanges();
				var result = db.Maintenances.ToList();
				foreach (var item in result)
				{
					Truck truck = db.Trucks.Where(x => x.Id == item.TruckId).FirstOrDefault();
					TypeMaintenance type = db.TypeMaintenances.Where(x => x.Id == item.TypeId).FirstOrDefault();
					Driver driver = db.Drivers.Where(x => x.Id == item.DriverId).FirstOrDefault();
					Dispatcher dispatcher = db.Dispatchers.Where(x => x.Id == item.DispatcherId).FirstOrDefault();
					Mechanical mechanical = db.Mechanicals.Where(x => x.Id == item.MechanicalId).FirstOrDefault();

					TruckMaintenance maintenance = new TruckMaintenance();
					maintenance.Id = item.Id;
					maintenance.Truck = truck.Number.ToString();
					maintenance.Type = type.Description;
					maintenance.Driver = String.Format("{0} {1}", driver.Name, driver.LastName);
					maintenance.Dispatcher = String.Format("{0} {1}", dispatcher.Name, dispatcher.LastName);
					maintenance.DueDate = item.DueDate;
					maintenance.Mechanical = String.Format("{0} {1}", mechanical.Name, mechanical.LastName);
					maintenances.Add(maintenance);
				}
			}
			return maintenances;
		}
	}
}
