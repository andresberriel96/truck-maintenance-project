﻿using ApiTruckMaintenance.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiTruckMaintenance.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class CatalogosController : ControllerBase
	{
		TruckMaintenanceContext db = new TruckMaintenanceContext();

		[HttpGet]
		[Route("Trucks")]
		public List<Catalogo> GetTrucks()
		{
			var catalogo = new List<Catalogo>();
			foreach (var item in db.Trucks.ToList())
			{
				var itemCatalogo = new Catalogo();
				itemCatalogo.Id = item.Id;
				itemCatalogo.Descripcion = String.Format("Truck number: {0}", item.Number);
				catalogo.Add(itemCatalogo);
			}
			return catalogo;
		}

		[HttpGet]
		[Route("Types")]
		public List<Catalogo> GetTypesMaintenance()
		{
			var catalogo = new List<Catalogo>();
			foreach (var item in db.TypeMaintenances.ToList())
			{
				var itemCatalogo = new Catalogo();
				itemCatalogo.Id = item.Id;
				itemCatalogo.Descripcion = item.Description;
				catalogo.Add(itemCatalogo);
			}
			return catalogo;
		}

		[HttpGet]
		[Route("Drivers")]
		public List<Catalogo> GetDrivers()
		{
			var catalogo = new List<Catalogo>();
			foreach (var item in db.Drivers.ToList())
			{
				var itemCatalogo = new Catalogo();
				itemCatalogo.Id = item.Id;
				itemCatalogo.Descripcion = String.Format("{0} {1}", item.Name, item.LastName);
				catalogo.Add(itemCatalogo);
			}
			return catalogo;
		}

		[HttpGet]
		[Route("Dispatchers")]
		public List<Catalogo> GetDispatchers()
		{
			var catalogo = new List<Catalogo>();
			foreach (var item in db.Dispatchers.ToList())
			{
				var itemCatalogo = new Catalogo();
				itemCatalogo.Id = item.Id;
				itemCatalogo.Descripcion = String.Format("{0} {1}", item.Name, item.LastName);
				catalogo.Add(itemCatalogo);
			}
			return catalogo;
		}

		[HttpGet]
		[Route("Mechanicals")]
		public List<Catalogo> GetMechanicals()
		{
			var catalogo = new List<Catalogo>();
			foreach (var item in db.Mechanicals.ToList())
			{
				var itemCatalogo = new Catalogo();
				itemCatalogo.Id = item.Id;
				itemCatalogo.Descripcion = String.Format("{0} {1}", item.Name, item.LastName);
				catalogo.Add(itemCatalogo);
			}
			return catalogo;
		}
	}
}
