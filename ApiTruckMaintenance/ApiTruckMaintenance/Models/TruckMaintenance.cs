﻿namespace ApiTruckMaintenance.Models
{
	public class TruckMaintenance
	{
		public int Id { get; set; }
		public string Truck { get; set; }
		public string Type { get; set; }
		public string Driver { get; set; }
		public string Dispatcher { get; set; }
		public DateTime? DueDate { get; set; }
		public string Mechanical { get; set; }
	}
}
