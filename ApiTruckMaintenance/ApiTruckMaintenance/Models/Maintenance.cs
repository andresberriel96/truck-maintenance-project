﻿using System;
using System.Collections.Generic;

namespace ApiTruckMaintenance.Models;

public partial class Maintenance
{
    public int Id { get; set; }

    public int? TruckId { get; set; }

    public int? TypeId { get; set; }

    public int? DriverId { get; set; }

    public int? DispatcherId { get; set; }

    public DateTime? DueDate { get; set; }

    public int? MechanicalId { get; set; }

    public virtual Dispatcher? Dispatcher { get; set; }

    public virtual Driver? Driver { get; set; }

    public virtual Mechanical? Mechanical { get; set; }

    public virtual Truck? Truck { get; set; }

    public virtual TypeMaintenance? Type { get; set; }
}
