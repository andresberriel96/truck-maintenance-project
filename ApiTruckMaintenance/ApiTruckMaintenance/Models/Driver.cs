﻿using System;
using System.Collections.Generic;

namespace ApiTruckMaintenance.Models;

public partial class Driver
{
    public int Id { get; set; }

    public string? Name { get; set; }

    public string? LastName { get; set; }

    public int? Age { get; set; }

    public virtual ICollection<Maintenance> Maintenances { get; } = new List<Maintenance>();
}
