﻿using System;
using System.Collections.Generic;

namespace ApiTruckMaintenance.Models;

public partial class Mechanical
{
    public int Id { get; set; }

    public string? Name { get; set; }

    public string? LastName { get; set; }

    public virtual ICollection<Maintenance> Maintenances { get; } = new List<Maintenance>();
}
