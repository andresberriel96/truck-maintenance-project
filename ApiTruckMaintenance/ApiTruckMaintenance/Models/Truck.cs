﻿using System;
using System.Collections.Generic;

namespace ApiTruckMaintenance.Models;

public partial class Truck
{
    public int Id { get; set; }

    public int? Number { get; set; }

    public string? Model { get; set; }

    public virtual ICollection<Maintenance> Maintenances { get; } = new List<Maintenance>();
}
