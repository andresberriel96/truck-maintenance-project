﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace ApiTruckMaintenance.Models;

public partial class TruckMaintenanceContext : DbContext
{
    public TruckMaintenanceContext()
    {
    }

    public TruckMaintenanceContext(DbContextOptions<TruckMaintenanceContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Dispatcher> Dispatchers { get; set; }

    public virtual DbSet<Driver> Drivers { get; set; }

    public virtual DbSet<Maintenance> Maintenances { get; set; }

    public virtual DbSet<Mechanical> Mechanicals { get; set; }

    public virtual DbSet<Truck> Trucks { get; set; }

    public virtual DbSet<TypeMaintenance> TypeMaintenances { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("data source=DESKTOP-E03Q563\\SQLEXPRESS;initial catalog=TruckMaintenance;user id=sa;password=Berriel123;Trusted_Connection=True;TrustServerCertificate=True;");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Dispatcher>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Dispatch__3214EC079F8C1914");

            entity.ToTable("Dispatcher");

            entity.Property(e => e.LastName)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.Name)
                .HasMaxLength(50)
                .IsUnicode(false);
        });

        modelBuilder.Entity<Driver>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Driver__3214EC07311C6245");

            entity.ToTable("Driver");

            entity.Property(e => e.LastName)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.Name)
                .HasMaxLength(50)
                .IsUnicode(false);
        });

        modelBuilder.Entity<Maintenance>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Maintena__3214EC07E3E5C10F");

            entity.Property(e => e.DueDate).HasColumnType("date");

            entity.HasOne(d => d.Dispatcher).WithMany(p => p.Maintenances)
                .HasForeignKey(d => d.DispatcherId)
                .HasConstraintName("FK__Maintenan__Dispa__2F10007B");

            entity.HasOne(d => d.Driver).WithMany(p => p.Maintenances)
                .HasForeignKey(d => d.DriverId)
                .HasConstraintName("FK__Maintenan__Drive__2E1BDC42");

            entity.HasOne(d => d.Mechanical).WithMany(p => p.Maintenances)
                .HasForeignKey(d => d.MechanicalId)
                .HasConstraintName("FK__Maintenan__Mecha__300424B4");

            entity.HasOne(d => d.Truck).WithMany(p => p.Maintenances)
                .HasForeignKey(d => d.TruckId)
                .HasConstraintName("FK__Maintenan__Trcuk__2C3393D0");

            entity.HasOne(d => d.Type).WithMany(p => p.Maintenances)
                .HasForeignKey(d => d.TypeId)
                .HasConstraintName("FK__Maintenan__TypeI__2D27B809");
        });

        modelBuilder.Entity<Mechanical>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Mechanic__3214EC07555FC0DF");

            entity.ToTable("Mechanical");

            entity.Property(e => e.LastName)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.Name)
                .HasMaxLength(50)
                .IsUnicode(false);
        });

        modelBuilder.Entity<Truck>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Truck__3214EC0753AD7605");

            entity.ToTable("Truck");

            entity.Property(e => e.Model)
                .HasMaxLength(50)
                .IsUnicode(false);
        });

        modelBuilder.Entity<TypeMaintenance>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__TypeMain__3214EC0760AF5E34");

            entity.ToTable("TypeMaintenance");

            entity.Property(e => e.Description)
                .HasMaxLength(100)
                .IsUnicode(false);
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
