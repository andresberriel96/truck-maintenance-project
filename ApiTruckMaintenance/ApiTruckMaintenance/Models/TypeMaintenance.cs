﻿using System;
using System.Collections.Generic;

namespace ApiTruckMaintenance.Models;

public partial class TypeMaintenance
{
    public int Id { get; set; }

    public string? Description { get; set; }

    public virtual ICollection<Maintenance> Maintenances { get; } = new List<Maintenance>();
}
