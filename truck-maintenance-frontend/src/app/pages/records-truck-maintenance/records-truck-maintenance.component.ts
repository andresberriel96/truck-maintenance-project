import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TruckMaintenance } from 'src/app/Models/TruckMaintenance';
import { MaintenanceService } from 'src/app/Services/maintenance.service';

@Component({
  selector: 'app-records-truck-maintenance',
  templateUrl: './records-truck-maintenance.component.html',
  styles: [
  ]
})
export class RecordsTruckMaintenanceComponent implements OnInit {

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  dataSource = new MatTableDataSource<TruckMaintenance>();
  columns: String[] = ["truck", "type", "driver", "dispatcher", "dueDate", "mechanical", "edit", "delete" ];
  truckMaintenances: TruckMaintenance[] = [];


  constructor(private maintenanceService: MaintenanceService) { 
    this.getMaintenances();
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.paginator._intl.itemsPerPageLabel = 'Items per page';
    this.paginator._intl.nextPageLabel = 'Next page';
    this.paginator._intl.lastPageLabel = 'Last page';
    this.paginator._intl.previousPageLabel = 'Previous page';
    this.paginator._intl.firstPageLabel = 'Fist page';
  }

  getMaintenances(){
    this.maintenanceService.getMaintenances().subscribe((resp: any) => {
      console.log(resp);
      this.truckMaintenances = resp;
      this.dataSource = new MatTableDataSource<TruckMaintenance>(this.truckMaintenances);
      this.dataSource.paginator = this.paginator;
    });
  }

  deleteItem(id: number){
    console.log(id);
    this.maintenanceService.deleteMaintenances(id).subscribe((resp: any) => {
      this.truckMaintenances = resp;
      this.dataSource = new MatTableDataSource<TruckMaintenance>(this.truckMaintenances);
      this.dataSource.paginator = this.paginator;
    });
  }

}
