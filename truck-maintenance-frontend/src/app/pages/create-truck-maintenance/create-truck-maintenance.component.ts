import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { DialogComponent } from 'src/app/components/dialog/dialog.component';
import { Catalogo } from 'src/app/Models/catalogo';
import { MaintenanceRequest } from 'src/app/Models/MaintenanceRequets';
import { CatalogoService } from 'src/app/Services/catalogo.service';
import { MaintenanceService } from 'src/app/Services/maintenance.service';

@Component({
  selector: 'app-create-truck-maintenance',
  templateUrl: './create-truck-maintenance.component.html',
  styles: [
  ]
})
export class CreateTruckMaintenanceComponent implements OnInit {

  trucks: Catalogo[] = [];
  types: Catalogo[] = [];
  drivers: Catalogo[] = [];
  dispatchers: Catalogo[] = [];
  mechanicals: Catalogo[] = [];
  isEdit: boolean = false;
  id: number = 0;

  formMaintenance = this.fb.group({
    truckId: [0, Validators.compose([Validators.required, Validators.min(1)])],
    typeId: [0, Validators.compose([Validators.required, Validators.min(1)])],
    driverId: [0, Validators.compose([Validators.required, Validators.min(1)])],
    dispatcherId: [0, Validators.compose([Validators.required, Validators.min(1)])],
    dueDate: ['', Validators.compose([Validators.required])],
    mechanicalId: [0, Validators.compose([Validators.required, Validators.min(1)])]
  });

  constructor(private fb: FormBuilder, private catalogoService: CatalogoService, private maintenanceService: MaintenanceService, private dialog: MatDialog, private router: ActivatedRoute) { 
    this.getTrucks();
    this.getTypes();
    this.getDrivers();
    this.getDispatchers();
    this.getMechanicals();
    this.getIdEdit();
  }

  ngOnInit(): void {
  }

  getTrucks(){
    this.catalogoService.getTrucks().subscribe((resp: any) => this.trucks = resp);
  }

  getTypes(){
    this.catalogoService.getTypes().subscribe((resp: any) => this.types = resp);
  }

  getDrivers(){
    this.catalogoService.getDrivers().subscribe((resp: any) => this.drivers = resp);
  }

  getDispatchers(){
    this.catalogoService.getDispatchers().subscribe((resp: any) => this.dispatchers = resp);
  }

  getMechanicals(){
    this.catalogoService.getMechanicals().subscribe((resp: any) => this.mechanicals = resp);
  }

  createMaintenance(){
    if(this.formMaintenance.valid){
      if(!this.isEdit){
        this.maintenanceService.addMaintenance(this.formMaintenance.value).subscribe((resp: any) => {
          DialogComponent.openDialog('Success', resp, this.dialog);
          this.reset();
        });
      } else {
        this.updateMaintenance();
      }
    } else {
      DialogComponent.openDialog('Warning', 'Invalid Form', this.dialog);
    }
  }
  
  updateMaintenance(){
    this.maintenanceService.updateMaintenance(this.formMaintenance.value, this.id).subscribe((resp: any) => {
      DialogComponent.openDialog('Success', resp, this.dialog);
      console.log(resp)
      this.reset();
    });
  }

  reset(){
    this.formMaintenance.reset();
    this.isEdit = false;
    this.formMaintenance = this.fb.group({
      truckId: [0, Validators.compose([Validators.required, Validators.min(1)])],
      typeId: [0, Validators.compose([Validators.required, Validators.min(1)])],
      driverId: [0, Validators.compose([Validators.required, Validators.min(1)])],
      dispatcherId: [0, Validators.compose([Validators.required, Validators.min(1)])],
      dueDate: ['', Validators.compose([Validators.required])],
      mechanicalId: [0, Validators.compose([Validators.required, Validators.min(1)])]
    });
  }

  getIdEdit(){
    if(this.router.snapshot.params.id){
      this.id = this.router.snapshot.params.id;
      this.isEdit = true;
      this.maintenanceService.getMaintenance(this.id).subscribe((resp: any) => {
        this.formMaintenance = this.fb.group({
          truckId: [resp.truckId, Validators.compose([Validators.required, Validators.min(1)])],
          typeId: [resp.typeId, Validators.compose([Validators.required, Validators.min(1)])],
          driverId: [resp.driverId, Validators.compose([Validators.required, Validators.min(1)])],
          dispatcherId: [resp.dispatcherId, Validators.compose([Validators.required, Validators.min(1)])],
          dueDate: [resp.dueDate, Validators.compose([Validators.required])],
          mechanicalId: [resp.mechanicalId, Validators.compose([Validators.required, Validators.min(1)])]
        });
        this.formMaintenance.get('dueDate')?.patchValue(this.formatDate(resp.dueDate));
        console.log(resp);
      });
    }
  }

  private formatDate(date: string) {
    const d = new Date(date);
    let month = '' + (d.getMonth() + 1);
    let day = '' + d.getDate();
    const year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }
}
