import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateTruckMaintenanceComponent } from './pages/create-truck-maintenance/create-truck-maintenance.component';
import { RecordsTruckMaintenanceComponent } from './pages/records-truck-maintenance/records-truck-maintenance.component';

const routes: Routes = [
  {
    path: '',
    component: CreateTruckMaintenanceComponent
  },
  {
    path: 'create',
    component: CreateTruckMaintenanceComponent
  },
  {
    path: 'create/:id',
    component: CreateTruckMaintenanceComponent
  },
  {
    path: 'records',
    component: RecordsTruckMaintenanceComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
