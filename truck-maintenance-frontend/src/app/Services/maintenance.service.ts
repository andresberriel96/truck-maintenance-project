import { DatePipe, formatDate } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { MaintenanceRequest } from '../Models/MaintenanceRequets';
import { TruckMaintenance } from '../Models/TruckMaintenance';

const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})
export class MaintenanceService {

  constructor(private http: HttpClient) { }

  getMaintenances(){
    return this.http.get<TruckMaintenance>(`${ base_url }/TruckMaintenance`);
  }

  getMaintenance(id: number){
    return this.http.get<TruckMaintenance>(`${ base_url }/TruckMaintenance/${ id }`);
  }


  deleteMaintenances(id: number){
    return this.http.delete(`${ base_url }/TruckMaintenance/Delete/${id}`);
  }

  addMaintenance(request: MaintenanceRequest){
    return this.http.post(`${ base_url }/TruckMaintenance/Create`, request);
  }

  updateMaintenance(request: MaintenanceRequest, id: number){
    request.id = id;
    request.dueDate = request.dueDate.substring(0, 10);
    return this.http.put(`${ base_url }/TruckMaintenance/Update`, request);
  }
}
