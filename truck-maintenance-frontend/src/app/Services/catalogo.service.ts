import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})
export class CatalogoService {

  constructor(private http: HttpClient) { }

  getTrucks(){
    return this.http.get(`${ base_url }/Catalogos/Trucks`);
  }

  getTypes(){
    return this.http.get(`${ base_url }/Catalogos/Types`);
  }

  getDrivers(){
    return this.http.get(`${ base_url }/Catalogos/Drivers`);
  }

  getDispatchers(){
    return this.http.get(`${ base_url }/Catalogos/Dispatchers`);
  }

  getMechanicals(){
    return this.http.get(`${ base_url }/Catalogos/Mechanicals`);
  }
}
