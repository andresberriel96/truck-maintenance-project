export class MaintenanceRequest {
    id: number;
    truckId: number;
    typeId: number;
    driverId: number;
    dispatcherId: number;
    dueDate: string;
    mechanicalId: number;

    constructor(request: MaintenanceRequest){
        this.id = request.id;
        this.truckId = request.truckId;
        this.typeId = request.typeId;
        this.driverId = request.driverId;
        this.dispatcherId = request.dispatcherId;
        this.dueDate = request.dueDate;
        this.mechanicalId = request.mechanicalId;
    }
}