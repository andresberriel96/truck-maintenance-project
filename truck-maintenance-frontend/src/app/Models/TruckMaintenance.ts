export class TruckMaintenance {
    id: number;
    truck: String;
    type: String;
    driver: String;
    dispatcher: String;
    dueDate: Date;
    mechanical: String;

    constructor(truckMaintenance: TruckMaintenance){
        this.id = truckMaintenance.id;
        this.truck = truckMaintenance.truck;
        this.type = truckMaintenance.type;
        this.driver = truckMaintenance.driver;
        this.dispatcher = truckMaintenance.dispatcher;
        this.dueDate = truckMaintenance.dueDate;
        this.mechanical = truckMaintenance.mechanical;
    }

}