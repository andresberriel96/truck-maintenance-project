export class Catalogo {
    id: number;
    descripcion: string;

    constructor(catalogo: Catalogo) {
        this.id = catalogo.id;
        this.descripcion = catalogo.descripcion;
    }
    
}