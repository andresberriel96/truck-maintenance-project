import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface DataDialog {
  titulo: String,
  mensaje: String
}

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: []
})
export class DialogComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public data: DataDialog) {
  }

  static openDialog(titulo: String, mensaje: String, dialog: MatDialog){
    return dialog.open(DialogComponent,{
      data: { titulo: titulo, mensaje: mensaje }
    });
  }


}
